# python inference_ablation.py --gpu_index=0 --img_dir=../../../data/REFUGE2/Validation-400-images/ --is_test=True
import os
import argparse
import time

import numpy as np
import pandas as pd
from PIL import Image

from model.efficient_net import EfficientNetB0, EfficientNetB4, set_output

import utils

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--gpu_index',
    type=str,
    required=True
    )
parser.add_argument(
    '--img_dir',
    type=str,
    required=True
    )
parser.add_argument(
    '--is_test',
    type=str,
    required=False,
    default="False"
    )  
FLAGS, _ = parser.parse_known_args()

# misc params
os.environ['CUDA_VISIBLE_DEVICES'] = FLAGS.gpu_index
vessel_segmentor_model_path = "/home/vuno/development/REFUGE2/fovea_localization/vessel_segmentation/model/vessel"
input_size = (640, 640, 3)
final_featuremap_resolution = 80
if FLAGS.is_test == "False":
    out_dir_home="/home/vuno/development/REFUGE2/fovea_localization/submission/ablation_refuge_train/refuge_finetune_ensemble"
else:
    out_dir_home="/home/vuno/development/REFUGE2/fovea_localization/submission/ablation_refuge_train/refuge_finetune_ensemble_test"
out_coords_plot = os.path.join(out_dir_home, "coords_plot")
utils.makedirs(out_coords_plot)

# load a network
list_weight_path = ["/home/vuno/development/REFUGE2/fovea_localization/model/ablation_study_using_REFUGE_train_data_only/weight_{}epoch.h5".format(epoch) for epoch in range(20,30)] 
list_network = []
for weight_path in list_weight_path:
    fundus_network = EfficientNetB4(include_top=False, weights=None, use_resnet_init_conv=True, input_shape=input_size)
    vessel_network = EfficientNetB0(include_top=False, weights=None, use_resnet_init_conv=True, input_shape=input_size)
    network = set_output(fundus_network, vessel_network, fundus_network_resolution=(final_featuremap_resolution, final_featuremap_resolution))
    network.load_weights(weight_path)
    print("model loaded from {}".format(weight_path))
    list_network.append(network)
vessel_segmentor = utils.load_network(vessel_segmentor_model_path)

# run inference
filepaths = utils.all_files_under(FLAGS.img_dir)
list_Y, list_X = [], []
for filepath in filepaths:
    print("processing {}...".format(filepath))

    # prepare input
    img = np.array(Image.open(filepath))
    h_ori, w_ori, _ = img.shape
    resized_img = utils.resize_img(img, input_size[0], input_size[1])
    standardized_resized_img = utils.standardize_fundus(resized_img)
    network_input = np.expand_dims(standardized_resized_img, axis=0)
    vessel_segmentation = vessel_segmentor.predict(network_input)
    
    # run 
    Y_preds, X_preds=[], []
    for network in list_network:
        confidence_final, cy_final, cx_final, confidence_vessel, cy_vessel, cx_vessel = network.predict([network_input,  np.repeat((vessel_segmentation * 255).astype(np.uint8) / 255., 3, axis=-1)])
        # convert outputs to coords in ori img
        target = confidence_final[0, ..., 0]
        print(os.path.basename(filepath), np.max(target))
        index_max = np.argwhere(target.max() == target)[0]
        Y = (index_max[0] + cy_final[0, index_max[0], index_max[1], 0]) * h_ori / final_featuremap_resolution
        X = (index_max[1] + cx_final[0, index_max[0], index_max[1], 0]) * w_ori / final_featuremap_resolution
        Y_preds.append(Y)
        X_preds.append(X)

    # plot coords
    ensemble_Y = np.mean(Y_preds)
    ensemble_X = np.mean(X_preds)
    list_Y.append(ensemble_Y)
    list_X.append(ensemble_X)
    img[int(ensemble_Y)-30:int(ensemble_Y)+30, int(ensemble_X)-30:int(ensemble_X)+30,:]=255
    Image.fromarray(img.astype(np.uint8)).save(os.path.join(out_coords_plot, os.path.basename(filepath)))


# save inference results
df = pd.DataFrame({"ImageName":[os.path.basename(fpath) for fpath in filepaths], "Fovea_X":list_X, "Fovea_Y":list_Y})
df.to_csv(os.path.join(out_dir_home, "fovea_location_results.csv"), index=False)

