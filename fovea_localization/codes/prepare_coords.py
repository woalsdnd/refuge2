import pandas as pd
from PIL import Image
import numpy as np
import os

# REFUGE train
df_REFUGE_train = pd.read_excel("/home/vuno/development/data/REFUGE/Fovea_location_train.xlsx")
list_id, list_X, list_Y = [], [], []
for index, row in df_REFUGE_train.iterrows():
    fpath = os.path.join("/home/vuno/development/data/REFUGE/train_images/", row["ImgName"])
    list_id.append(row["ImgName"].replace(".jpg", ""))
    img = np.array(Image.open(fpath))
    h, w, _ = img.shape
    list_X.append(640.*row["Fovea_X"]/w)
    list_Y.append(640.*row["Fovea_Y"]/h)
# REFUGE val
df_REFUGE_val = pd.read_excel("/home/vuno/development/data/REFUGE/Fovea_locations_val.xlsx")
for index, row in df_REFUGE_val.iterrows():
    fpath = os.path.join("/home/vuno/development/data/REFUGE/val_cropped_images/", row["ImgName"].replace(".jpg", ".png"))
    list_id.append(row["ImgName"].replace(".jpg", ""))
    img = np.array(Image.open(fpath))
    h, w, _ = img.shape
    list_X.append(640.*row["Fovea_X"]/w)
    list_Y.append(640.*row["Fovea_Y"]/h)
# REFUGE test
df_REFUGE_test = pd.read_excel("/home/vuno/development/data/REFUGE/Fovea_locations_test.xlsx")
for index, row in df_REFUGE_test.iterrows():
    fpath = os.path.join("/home/vuno/development/data/REFUGE/Test400/", row["ImgName"].replace(".jpg", ".png"))
    list_id.append(row["ImgName"].replace(".jpg", ""))
    img = np.array(Image.open(fpath))
    h, w, _ = img.shape
    list_X.append(640.*row["Fovea_X"]/w)
    list_Y.append(640.*row["Fovea_Y"]/h)
df_REFUGE = pd.DataFrame({"id":list_id, "X": list_X, "Y":list_Y})
df_REFUGE.to_csv("/home/vuno/development/data/REFUGE/Fovea_location_640x640.csv", index=False)

