import os
import logging
import argparse
import configparser
import time

import keras
import numpy as np
import pandas as pd
from PIL import Image

import utils
import iterator_shared_array
import models

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--config_file',
    type=str,
    required=True
    )
FLAGS, _ = parser.parse_known_args()

# set config
config = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
config.read_file(open(FLAGS.config_file))
dir_training_input_data1 = config["Path"]["path_data1"]
dir_training_input_data2 = config["Path"]["path_data2"]
dir_training_input_data3 = config["Path"]["path_data3"]
dir_training_input_label1 = config["Path"]["path_label1"]
dir_training_input_label2 = config["Path"]["path_label2"]
dir_training_input_label3 = config["Path"]["path_label3"]
dir_val_input_data = config["Path"]["path_val_data"]
dir_val_input_label = config["Path"]["path_val_label"]
dir_input_check = config["Path"]["dir_input_check"]
dir_save_model = config["Path"]["dir_save_model"]
path_load_model = config["Path"]["path_load_model"] if "path_load_model" in config["Path"] else None
path_pretrained_weight = config["Path"]["path_pretrained_weight"] if "path_pretrained_weight" in config["Path"] else None
dir_experimental_result = config["Path"]["dir_experimental_result"]
dir_logger = config["Path"]["dir_logger"]
path_logger = os.path.join(dir_logger, os.path.basename(FLAGS.config_file).replace(".cfg", ".log")) 
batch_size = int(config["Train"]["batch_size"])
n_epochs = int(config["Train"]["n_epochs"])
lr_decay_tolerance = int(config["Train"]["lr_decay_tolerance"])
lr_decay_factor = float(config["Train"]["lr_decay_factor"])
lr_start_value = float(config["Train"]["lr_start"])
lr_min_value = float(config["Train"]["lr_min_value"])
optimizer = config["Train"]["optimizer"]
gpu_index = config["Train"]["gpu_index"]
input_size = (int(config["Input"]["height"]), int(config["Input"]["width"]), int(config["Input"]["depth"]))
utils.makedirs(dir_save_model)
utils.makedirs(dir_experimental_result)
utils.makedirs(dir_logger)

# set logger
logger = logging.getLogger()
logger.setLevel(logging.INFO)
formatter = logging.Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s', datefmt='[%Y-%m-%d %H:%M:%S]')
handler = logging.FileHandler(path_logger)
handler.setFormatter(formatter)
logger.addHandler(handler)

# set gpu index
os.environ['CUDA_VISIBLE_DEVICES'] = gpu_index

# split data
path_data1 = utils.all_files_under(dir_training_input_data1)
path_data2 = utils.all_files_under(dir_training_input_data2)
path_data3 = utils.all_files_under(dir_training_input_data3)
path_val_data = utils.all_files_under(dir_val_input_data)
df_label1 = pd.read_csv(dir_training_input_label1)
df_label2 = pd.read_csv(dir_training_input_label2)
df_label3 = pd.read_csv(dir_training_input_label3)
df_val_label = pd.read_csv(dir_val_input_label)
list_path_train_data = path_data1 + path_data2 + path_data3
df_train_label = utils.merge_dr_label_open_data([df_label1, df_label2, df_label3])

# set filenames and labels
train_input_data, train_label_data = [], []
for fpath in list_path_train_data:
    fid = utils.drop_extension(os.path.basename(fpath))
    if fid in df_train_label.index:
        label = df_train_label.loc[fid, "label"]
        train_input_data.append(fpath)
        train_label_data.append(label)
val_input_data, val_label_data = [], []
df_val_label = utils.merge_dr_label_open_data([df_val_label])
for fpath in path_val_data:
    fid = utils.drop_extension(os.path.basename(fpath))
    if fid in df_val_label.index:
        label = df_val_label.loc[fid, "label"]
        val_input_data.append(fpath)
        val_label_data.append(label)

# set iterator
class_weight, sample_weight = utils.balanced_class_weights(train_label_data)
logger.info("class weight: {}".format(class_weight))
train_batch_fetcher = iterator_shared_array.BatchFetcher((train_input_data, train_label_data), sample_weight,
                                           utils.fundus_classification_processing_func_train, batch_size, shuffle=True, replace=True, shared_array_shape=[input_size])
val_batch_fetcher = iterator_shared_array.BatchFetcher((val_input_data, val_label_data), None,
                                          utils.fundus_classification_processing_func_val, batch_size, shuffle=False, replace=False, shared_array_shape=[input_size])

# define network
if path_load_model:
    network = utils.load_network(path_load_model, trainable=True)
    network = models.set_optimizer(network)
    logger.info("model loaded from {}".format(path_load_model))
else:
    network = models.network(input_size, lr_start_value, optimizer)
    if path_pretrained_weight:
        network.load_weights(path_pretrained_weight)
network.summary()
# save network
with open(os.path.join(dir_save_model, "network.json"), 'w') as f:
    f.write(network.to_json())

lr_scheduler = utils.LrScheduler(lr_object=network.optimizer.lr, lr_start_value=lr_start_value,
                                 lr_min_value=lr_min_value, lr_decay_tolerance=lr_decay_tolerance,
                                 lr_decay_factor=lr_decay_factor, logger=logger, score_func=lambda x:x[0])
for epoch in range(n_epochs):
    # train loop
    list_training_loss, list_training_acc = [], []
    for data, list_arr in train_batch_fetcher:
#         utils.fundus_classification_check_data(batch, os.path.join(dir_input_check, "train"), phase="training")
        loss, acc = network.train_on_batch(list_arr[0], data[1])
        utils.stack_list(head1=list_training_loss, tail1=[loss] * len(batch[0]), head2=list_training_acc, tail2=[acc] * len(batch[0]))
    train_metrics = {"loss":np.mean(list_training_loss), "acc":np.mean(list_training_acc)}
    utils.log_summary(logger, phase="training", epoch=epoch, **train_metrics)
    
    # val loop
    list_gt, list_pred = [], []
    for data, list_arr in val_batch_fetcher:
#         utils.fundus_classification_check_data(batch, os.path.join(dir_input_check, "val"), phase="val")
        preds = network.predict(list_arr[0])
        utils.stack_list(head1=list_gt, tail1=list(batch[2]), head2=list_pred, tail2=list(utils.float2class(preds, 0, 4)))
    val_metrics = utils.categorical_stats(list_gt, list_pred, weighted_kappa=True)
    utils.log_summary(logger, phase="validation", epoch=epoch, **val_metrics)
    
    # adjust learning rate
    lr_scheduler.adjust_lr(epoch, val_metrics)

    # save network
    network.save_weights(os.path.join(dir_save_model, "weight_{}epoch.h5".format(epoch)))
