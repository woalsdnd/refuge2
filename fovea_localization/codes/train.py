# python train.py  --config_file=../config/detection_use_all_data.cfg
import os
import logging
import argparse
import configparser
import time

import numpy as np
import pandas as pd
from PIL import Image

import utils
import iterator_shared_array
from model.efficient_net import EfficientNetB0, EfficientNetB4, set_output, set_optimizer_detection


def prepare_gt(batch_size, resolution, normalized_coords):
    segmap = np.zeros((batch_size, resolution, resolution, 1))
    cy_map = -np.ones((batch_size, resolution, resolution, 1))
    cx_map = -np.ones((batch_size, resolution, resolution, 1))
    for index in range(batch_size):
        if coords[index, 0]!=-1 and coords[index, 1]!=-1: # ignore illegal coordinates
            segmap[index][int(resolution * coords[index, 0]), int(resolution *coords[index, 1]), 0] = 1
            cy_map[index][int(resolution * coords[index, 0]), int(resolution *coords[index, 1]), 0] = resolution * coords[index, 0] - int(resolution * coords[index, 0]) 
            cx_map[index][int(resolution * coords[index, 0]), int(resolution *coords[index, 1]), 0] = resolution *coords[index, 1] - int(resolution *coords[index, 1])
    return segmap, cy_map, cx_map


def normalize_coords(coords, h, w):
    normalized_coords = np.copy(coords)
    normalized_coords[:,0]/=h
    normalized_coords[:,1]/=w

    # assign (-1, -1) for illegal coordinates
    normalized_coords[:,0][normalized_coords[:,0] < 1e-6]=-1
    normalized_coords[:,0][normalized_coords[:,0] > 1-1e-6]=-1
    normalized_coords[:,1][normalized_coords[:,1] < 1e-6]=-1
    normalized_coords[:,1][normalized_coords[:,1] > 1-1e-6]=-1

    return normalized_coords

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--config_file',
    type=str,
    required=True
    )
FLAGS, _ = parser.parse_known_args()

# set config
config = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
config.read_file(open(FLAGS.config_file))
list_fundus_dir = [config["Path"]["path_fundus{}".format(index)] for index in range(1,2)]
list_vessel_dir = [config["Path"]["path_vessel{}".format(index)] for index in range(1,2)]
list_coords_csv = [config["Path"]["path_label{}".format(index)] for index in range(1,2)]
dir_save_model = config["Path"]["dir_save_model"]
path_load_model = config["Path"]["path_load_model"] if "path_load_model" in config["Path"] else None
path_pretrained_weight = config["Path"]["path_pretrained_weight"] if "path_pretrained_weight" in config["Path"] else None
dir_experimental_result = config["Path"]["dir_experimental_result"]
dir_logger = config["Path"]["dir_logger"]
path_logger = os.path.join(dir_logger, os.path.basename(FLAGS.config_file).replace(".cfg", ".log")) 
batch_size = int(config["Train"]["batch_size"])
n_epochs = int(config["Train"]["n_epochs"])
lr_decay_tolerance = int(config["Train"]["lr_decay_tolerance"])
lr_decay_factor = float(config["Train"]["lr_decay_factor"])
lr_start_value = float(config["Train"]["lr_start"])
lr_min_value = float(config["Train"]["lr_min_value"])
optimizer = config["Train"]["optimizer"]
gpu_index = config["Train"]["gpu_index"]
input_size = (int(config["Input"]["height"]), int(config["Input"]["width"]), int(config["Input"]["depth"]))
final_featuremap_resolution = int(config["Output"]["final_featuremap_resolution"])
network_featuremap_resolution = int(config["Output"]["network_featuremap_resolution"])
utils.makedirs(dir_save_model)
utils.makedirs(dir_experimental_result)
utils.makedirs(dir_logger)

# set logger and tensorboard
logger = logging.getLogger()
logger.setLevel(logging.INFO)
formatter = logging.Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s', datefmt='[%Y-%m-%d %H:%M:%S]')
handler = logging.FileHandler(path_logger)
handler.setFormatter(formatter)
logger.addHandler(handler)
tensorboard = utils.CustomTensorBoard(
  log_dir=os.path.join(dir_logger, "tensorboard", os.path.basename(FLAGS.config_file).replace(".cfg", ".log")),
  write_graph=False,
  batch_size=batch_size
)

# set gpu index
os.environ['CUDA_VISIBLE_DEVICES'] = gpu_index

# split data
list_df_train=[]
for index in range(len(list_fundus_dir)):
    list_fundus_path = utils.all_files_under(list_fundus_dir[index])
    list_vessel_path = utils.all_files_under(list_vessel_dir[index])
    df_coords = pd.read_csv(list_coords_csv[index])
    df_fundus_path = pd.DataFrame({"fundus_path":list_fundus_path, "id":[os.path.basename(fpath).replace(".png", "") for fpath in list_fundus_path]})
    df_vessel_path = pd.DataFrame({"vessel_path":list_vessel_path, "id":[os.path.basename(fpath).replace(".png", "") for fpath in list_vessel_path]})
    df_merged = pd.merge(df_coords, df_fundus_path, on="id")
    df_merged = pd.merge(df_merged, df_vessel_path, on="id")
    list_df_train.append(df_merged)
df_train = pd.concat(list_df_train, ignore_index=True)
df_train["is_train"]=df_train["fundus_path"].map(lambda x: True if os.path.basename(x)[0]!="V" and os.path.basename(x)[0]!="T" else False)
df_train = df_train[df_train["is_train"]]
logger.info("#train: {}".format(len(df_train)))

# set filenames and labels and iterators
train_fundus_data, train_vessel_data, train_coords = df_train["fundus_path"], df_train["vessel_path"], np.array(df_train[["Y","X"]])
train_batch_fetcher = iterator_shared_array.BatchFetcher((train_fundus_data, train_vessel_data, train_coords), [1./len(train_fundus_data)]*len(train_fundus_data),
                                           utils.fundus_localization_processing_func_train, batch_size, sample=True, replace=True, shared_array_shape=[input_size, input_size[:2], (2,)])

# define network
if path_load_model:
    fundus_network = EfficientNetB4(include_top=False, weights=None, use_resnet_init_conv=True, input_shape=input_size)
    vessel_network = EfficientNetB0(include_top=False, weights=None, use_resnet_init_conv=True, input_shape=input_size)
    network = set_output(fundus_network, vessel_network, fundus_network_resolution=(final_featuremap_resolution, final_featuremap_resolution))
    network.load_weights(path_load_model)
    logger.info("model loaded from {}".format(path_load_model))
else:
    from keras.applications.resnet50 import ResNet50
    resnet = ResNet50(weights='imagenet')
    fundus_network = EfficientNetB4(include_top=False, weights=None, use_resnet_init_conv=True, input_shape=input_size)
    vessel_network = EfficientNetB0(include_top=False, weights=None, use_resnet_init_conv=True, input_shape=input_size)
    fundus_network.layers[1].set_weights([resnet.layers[2].get_weights()[0]])
    vessel_network.layers[1].set_weights([resnet.layers[2].get_weights()[0]])
    fundus_network_imagenet_pretrained = EfficientNetB4(include_top=False, weights="imagenet", input_shape=input_size)
    fundus_network = utils.copy_weights(fundus_network_imagenet_pretrained, fundus_network)
    vessel_network_imagenet_pretrained = EfficientNetB0(include_top=False, weights="imagenet", input_shape=input_size)
    vessel_network = utils.copy_weights(vessel_network_imagenet_pretrained, vessel_network)
    utils.makedirs(dir_save_model)
    fundus_network.save_weights(os.path.join(dir_save_model, "imagenet_pretrained_fundus.h5"))
    vessel_network.save_weights(os.path.join(dir_save_model, "imagenet_pretrained_vessel.h5"))
    network = set_output(fundus_network, vessel_network, fundus_network_resolution=(final_featuremap_resolution, final_featuremap_resolution))
    network.save_weights(os.path.join(dir_save_model, "network_imagenet_pretrained_weight.h5"))
network = set_optimizer_detection(network, [1,1,1,0.1,0.1,0.1])
network.summary()
tensorboard.set_model(network)  # set tensorboard callback associated with network

lr_scheduler = utils.LrScheduler(lr_object=network.optimizer.lr, lr_start_value=lr_start_value,
                                 lr_min_value=lr_min_value, lr_decay_tolerance=lr_decay_tolerance,
                                 lr_decay_factor=lr_decay_factor, logger=logger, score_func=lambda x:x["distance_final_640x640"])
list_train_loss_confidence_final, list_train_loss_cy_final, list_train_loss_cx_final, list_train_loss_confidence_vessel, list_train_loss_cy_vessel, list_train_loss_cx_vessel=[],[],[],[],[],[]
for epoch in range(n_epochs):
    # train loop
    list_training_loss, list_training_acc = [], []
    for data, list_arr in train_batch_fetcher:
        tensorboard.draw_imgs("Training Fundus", epoch, (list_arr[0]*30+127).astype(np.uint8), plot_once=True)
        tensorboard.draw_imgs("Training Vessel", epoch, (list_arr[1]*255).astype(np.uint8), plot_once=True)
        # prepare gt
        coords = normalize_coords(list_arr[2], input_size[0], input_size[1])
        bs = list_arr[0].shape[0]
        gt_final_segmap, gt_final_cy_map, gt_final_cx_map = prepare_gt(bs, final_featuremap_resolution, coords)
        gt_vessel_segmap, gt_vessel_cy_map, gt_vessel_cx_map = prepare_gt(bs, network_featuremap_resolution, coords)
        # train & record
        loss_total, loss_confidence_final, loss_cy_final, loss_cx_final, loss_confidence_vessel, loss_cy_vessel, loss_cx_vessel = network.train_on_batch([list_arr[0], np.repeat(np.expand_dims(list_arr[1], axis=-1), 3, axis=-1)], [gt_final_segmap, gt_final_cy_map, gt_final_cx_map, gt_vessel_segmap, gt_vessel_cy_map, gt_vessel_cx_map])    
        utils.stack_list(head1=list_train_loss_confidence_final, tail1=[loss_confidence_final] * bs, head2=list_train_loss_cy_final, tail2=[loss_cy_final] * bs, head3=list_train_loss_cx_final, tail3=[loss_cx_final] * bs, head4=list_train_loss_confidence_vessel, tail4=[loss_confidence_vessel] * bs,head5=list_train_loss_cy_vessel, tail5=[loss_cy_vessel] * bs,head6=list_train_loss_cx_vessel, tail6=[loss_cx_vessel] * bs,)
        
    train_metrics = {"confidence_final":np.mean(list_train_loss_confidence_final), "cy_final":np.mean(list_train_loss_cy_final), "cx_final":np.mean(list_train_loss_cx_final), "confidence_vesel":np.mean(list_train_loss_confidence_vessel), "cy_vessel":np.mean(list_train_loss_cy_vessel), "cx_vessel":np.mean(list_train_loss_cx_vessel)}
    utils.log_summary(logger, phase="training", epoch=epoch, **train_metrics)
    tensorboard.on_epoch_end(epoch, train_metrics)

    # save network
    network.save_weights(os.path.join(dir_save_model, "weight_{}epoch.h5".format(epoch)))
