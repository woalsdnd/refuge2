import os
import random
import time
import io

import numpy as np
import math
import pandas as pd
import matplotlib
import matplotlib.cm as cm
import tensorflow as tf

from sklearn.metrics import confusion_matrix
from sklearn.metrics.ranking import roc_auc_score
from sklearn.utils import class_weight
from PIL import Image, ImageEnhance, ImageFilter
from subprocess import Popen, PIPE
from skimage.transform import warp, AffineTransform
from skimage.util import random_noise
from scipy.misc import imresize

from keras.models import model_from_json
from keras import backend as K
from keras.callbacks import TensorBoard

from skimage import measure
from sklearn.manifold import TSNE
from sklearn.metrics import cohen_kappa_score
from sklearn.cross_decomposition import CCA

from albumentations.augmentations import transforms


########################################################
########################################################
############### GENERAL UTILITY CLASSES ################
########################################################
########################################################
class CustomTensorBoard(TensorBoard):
    """
    Custom Tensor Board class inheriting keras.callbacks.TensorBoard
    """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.has_plot_img = False

    def draw_imgs(self, tag, epoch, img_arr, plot_once=False):
        """
        Draw images in numpy array format. Assume epoch starts with 0.

        Args:
            tag: str
            epoch: int
            img_arr: (N, H, W, D)
            plot_once: bool

        Returns:
            None
            summary will be written in the log path

        Raises:
            AssertionError: wrong dtypes (tag, epoch, img_arr)
            AssertionError: img_arr is not 4-dimensional (N, H, W, D)

        """
        if (plot_once and not self.has_plot_img) or not plot_once:
            assert isinstance(tag, str) and isinstance(
                epoch, int) and isinstance(img_arr, np.ndarray)
            img_shape = img_arr.shape
            if len(img_shape) == 4:
                n, height, width, channel = img_shape
            elif len(img_shape) == 3:
                n, height, width = img_shape
                channel = 1
            writer = tf.summary.FileWriter(self.log_dir)
            for index in range(n):
                image = Image.fromarray(img_arr[index])
                with io.BytesIO() as f:
                    image.save(f, format='PNG')
                    image_string = f.getvalue()
                    tf_img = tf.Summary.Image(
                        height=height, width=width, colorspace=channel, encoded_image_string=image_string)
                    summary = tf.Summary(value=[tf.Summary.Value(
                        tag="{}/{}".format(tag, index), image=tf_img)])
                    writer.add_summary(summary, epoch)
            writer.close()
            self.has_plot_img = True


class LrScheduler:
    """
    Learning Rate Scheduler.
    Reduce on validation plateau and reset to init lr when the lr becomes less than minimum
    """

    def __init__(self, lr_object, lr_start_value, lr_min_value, lr_decay_tolerance, lr_decay_factor, logger, score_func):
        """
        Args:
            lr_object: keras optimizer lr object
            lr_start_value: float
            lr_min_value: float
            lr_decay_tolerance: int
            lr_decay_factor: float
            logger: logging object
            score_func: function that maps validation metric to scalar
        """
        self.lr_object = lr_object
        self.lr_start_value = lr_start_value
        self.lr_min_value = lr_min_value
        self.lr_decay_tolerance = lr_decay_tolerance
        self.lr_decay_factor = lr_decay_factor
        self.logger = logger
        self.best_val_score = None
        self.score_func = score_func
        self.n_no_improvements = 0

    def adjust_lr(self, epoch, val_metrics):
        curr_val_score = self.score_func(val_metrics)
        if self.best_val_score is None:  # first epoch
            self.best_val_score = curr_val_score
        elif self.best_val_score < curr_val_score:  # improved
            self.best_val_score = curr_val_score
            self.n_no_improvements = 0
        else:  # not improved
            self.n_no_improvements += 1
            if self.n_no_improvements == self.lr_decay_tolerance:
                new_lr = K.get_value(self.lr_object) * self.lr_decay_factor
                K.set_value(self.lr_object, new_lr)
                self.logger.info(
                    "set learning rate to {} at epoch {}".format(new_lr, epoch))
                self.n_no_improvements = 0

        # restart when lr is less than lr_min
        if K.get_value(self.lr_object) < self.lr_min_value:
            K.set_value(self.lr_object, self.lr_start_value)
            self.logger.info("reset learning rate to {} at epoch {}".format(self.lr_start_value, epoch))


########################################################
########################################################
############## GENERAL UTILITY FUNCTIONS ###############
########################################################
########################################################
def inside_box(point_interest, box_size):
    """
    return whether the point of interest (point_interest) is inside the box specified by box_size

    Args:
        point_interest: (array-like) 2d
        box_size: (array-like) 2d

    Returns:
        Whether the point of interest is inside the box (bool)

    """  
    return 0 <= point_interest[0] and point_interest[0] < box_size[0] and 0 <= point_interest[1] and point_interest[1] < box_size[1]

def count2ratio(data):
    """
    return a list of ratio from list (or array) of count

    Args:
        data: (array-like) count

    Returns:
        list of a ratio
        logs will be written in the file specified by the logger

    Examples:
        >>> count2ratio([4,3,2,1])
        >>> [0.4, 0.3, 0.2, 0.1]

    Raises:
        AssertionError: data in the format of list, tuple or ndarray

    """
    assert isinstance(data, (list, tuple, np.ndarray)
                      )  # data should be given as an array-like object

    return [1.*c / np.sum(data) for c in data]


def log_summary(logger, **kwargs):
    """
    Leave logs to logger.
    Reserved keyword: "phase", "epoch"

    Args:
        logger: logging object

    Returns:
        None
        logs will be written in the file specified by the logger

    """
    assert "phase" in kwargs.keys() and "epoch" in kwargs.keys(
    )  # training, validation, test info
    phase, epoch = kwargs["phase"], kwargs["epoch"]
    logging_msg = "{} at {}th epoch -- ".format(phase, epoch)
    for metric in [key for key in kwargs.keys() if key != "msg" and key != "phase" and key != epoch]:
        logging_msg = logging_msg + "{} : {}, ".format(metric, kwargs[metric])

    logger.info(logging_msg)


def stack_list(**kwargs):
    """
    Stack tail (list) to head (list)

    Args:
        head<num>, tail<num> (e.g. head1, tail1, head2, tail2, ...)

    Returns:
        None
        head lists will appended tail lists individually

    Raises:
        ValueError: head and tail should be list 
        AssertionError: head<num>, tail<num> should be given as keywords together

    """
    head_keywords = [key for key in kwargs.keys() if "head" in key]
    for head_keyword in head_keywords:
        tail_keyword = head_keyword.replace("head", "tail")
        assert tail_keyword in kwargs.keys()
        assert isinstance(kwargs[head_keyword], list)
        assert isinstance(kwargs[tail_keyword], list)
        kwargs[head_keyword] += kwargs[tail_keyword]


def compute_CCA(list_arr1, list_arr2):
    """
    Compute CCA between two list of arrays

    Args:
        list_arr1 (list of arrays with length M)
        list_arr2 (list of arrays with length N)

    Returns:
        CCA value in MxN matrix 

    """
    result = np.zeros((len(list_arr1), len(list_arr2)))
    for m in range(len(list_arr1)):
        for n in range(len(list_arr2)):
            min_dim = 1
            cca = CCA(n_components=min_dim)
            try:
                cca.fit(list_arr1[m], list_arr2[n])
                list_rho = []
                for i in range(min_dim):
                    list_rho.append(
                        pearsonr(cca.x_scores_[:, i], cca.y_scores_[:, i])[0])
                score = np.mean(list_rho)
            except:
                score = 0

            result[m, n] = score
    return result


def copy_weights(from_network, to_network):
    """
    Copy weights from "from_network" to "to_network"
    Weights are copied if the shape matches, first-come-first-served from lower layers

    Args:
        from_network: keras model from which weights are copied
        to_network: keras model to which weights are copied

    Returns:
        to_network: keras model with weights copied
    """
    from_layers_with_weights = [
        l for l in from_network.layers if len(l.get_weights()) > 0]
    to_layers_with_weights = [
        l for l in to_network.layers if len(l.get_weights()) > 0]

    from_layers_weight_shapes = []
    for from_layer in from_layers_with_weights:
        from_layers_weight_shapes.append(
            [w.shape for w in from_layer.get_weights()])
    to_layers_weight_shapes = []
    for to_layer in to_layers_with_weights:
        to_layers_weight_shapes.append(
            [w.shape for w in to_layer.get_weights()])

    curr_index_from_layer = 0
    for index_to_layer in range(len(to_layers_with_weights)):
        for index_from_layer in range(curr_index_from_layer, len(from_layers_with_weights)):
            to_layer_shapes = to_layers_weight_shapes[index_to_layer]
            from_layer_shapes = from_layers_weight_shapes[index_from_layer]
            if len(to_layer_shapes) == len(from_layer_shapes) and np.all([to_layer_shapes[index] == from_layer_shapes[index] for index in range(len(from_layer_shapes))]):
                to_layers_with_weights[index_to_layer].set_weights(
                    from_layers_with_weights[index_from_layer].get_weights())
                print("{} loaded from {}".format(
                    to_layers_with_weights[index_to_layer].name, from_layers_with_weights[index_from_layer].name))
                curr_index_from_layer = index_from_layer + 1
                break

    return to_network


def run_tsne(features):
    """
    T-SNE to 2d plane

    Args:
        features (N x D numpy array) -- N: #data, D: #dimension of a data

    Returns:
        embedded matrix (N x D numpy array) -- N: #data, D: #dimension of a data in 2d plane
    """
    embedded = TSNE(n_components=2, verbose=1,
                    n_iter=3000).fit_transform(features)
    return embedded


def rotate_scale_pt(origin, point, angle, scale=1):
    """
    Rotate a point counterclockwise by a given angle around a given origin in 2D plane.

    Args:
        origin: (oy, ox)
        point: (py,px)
        angle: angle in range of 0-360

    Returns:
        rotated point: (ry, rx)
    """
    rad = angle * math.pi / 180
    oy, ox = origin
    py, px = point
    qx = ox + math.cos(rad) * (px - ox)*scale - math.sin(rad) * (py - oy)*scale
    qy = oy + math.sin(rad) * (px - ox)*scale + math.cos(rad) * (py - oy)*scale
    return qy, qx


def drop_extension(fname):
    """
    Drop extension for the given filename 

    Args:
        fname (str): a name (or path) of the file

    Returns:
        fid (str): string without the extension 

    Examples:
        >>> drop_image_extension("jaemin.png")
        >>> jaemin
    """
    fid, _ = os.path.splitext(fname)
    return fid


def load_network(dir_path, trainable=False):
    """
    Load a network from a pair of a json and an h5 file

    Args:
        dir_path (str): path to the model files (1 json + 1 h5)
        trainable (boolean, optional): trainable parameters for keras networks

    Returns:
        network (keras network object): loaded network 

    Raises:
        AssertionError: one json file and one h5 file should exist under dir_path

    Examples:
        >>> load_network("./trained_models", trainable=True)
    """
    network_file = all_files_under(dir_path, extension=".json")
    weight_file = all_files_under(dir_path, extension=".h5")
    assert len(network_file) == 1 and len(weight_file) == 1
    with open(network_file[0], 'r') as f:
        network = model_from_json(f.read())
    network.load_weights(weight_file[0])
    network.trainable = trainable
    for l in network.layers:
        l.trainable = trainable
    return network


def keras_intermediate_output_func(network, list_input_layer_names, list_output_layer_names):
    """
    Return keras function for intermediate outputs

    Args:
        network (keras network object)
        list_input_layer_names (list of str): list of input layers in the network
        list_output_layer_names (list of str): list of output layers in the network

    Returns:
        func (keras function): a function that takes the inputs and generate outputs 

    Examples:
        >>> keras_intermediate_output_func(network, ["Input1", "Input2"], ["Output1", "Output2"])
    """
    list_inputs, list_outputs = [], []
    for input_layer_name in list_input_layer_names:
        list_inputs.append(network.get_layer(input_layer_name).input)
    for output_layer_name in list_output_layer_names:
        list_outputs.append(network.get_layer(output_layer_name).output)
    func = K.function(list_inputs, list_outputs)
    return func


def makedirs(dir_path):
    """
    Make directories if not exists

    Args:
        dir_path (str): path to the directory

    Returns:
        None 

    Examples:
        >>> makedirs("tmp")
    """
    if not os.path.isdir(dir_path):
        os.makedirs(dir_path)


def all_files_under(dir_path, extension=None, append_path=True, sort=True):
    """
    List all files under a given path

    Args:
        dir_path (str): path to the directory

    Returns:
        filenames (list of str): filenames if append_path=False or filepaths if append_path=True

    Examples:
        >>> all_files_under("tmp")
        >>> all_files_under("tmp", extension=".jpg")
        >>> all_files_under("tmp", extension=".jpg", append_path=False)
    """
    if append_path:
        if extension is None:
            filenames = [os.path.join(dir_path, fname)
                         for fname in os.listdir(dir_path)]
        else:
            filenames = [os.path.join(dir_path, fname) for fname in os.listdir(
                dir_path) if fname.endswith(extension)]
    else:
        if extension is None:
            filenames = [os.path.basename(fname)
                         for fname in os.listdir(dir_path)]
        else:
            filenames = [os.path.basename(fname) for fname in os.listdir(
                dir_path) if fname.endswith(extension)]

    if sort:
        filenames = sorted(filenames)

    return filenames


def load_imgs(path):
    """
    Load images from a list of filepath.

    Args:
        filepaths (list or array of str): a list of filepaths for images 

    Returns:
        numpy array for images (n_files, d1, d2, ...) 

    Raises:
        ValueError: path includes non-image file, images with irregular shape
    """
    filepaths = all_files_under(path) if isinstance(path, str) else path
    img_shape = image_shape(filepaths[0])
    images_arr = np.zeros((len(filepaths),) + img_shape, dtype=np.float32)
    for file_index in range(len(filepaths)):
        images_arr[file_index] = np.array(Image.open(filepaths[file_index]))
    return images_arr


def normalize(data, n_bit=8):
    """
    Normalize numpy array by dividing by 2**n_bit

    Args:
        data (numpy array)
        n_bit (int)

    Returns:
        normalized data (numpy array)
    """
    return 1.*data / (2 ** n_bit)


def standardize(data, axis=-1):
    """
    Standardize numpy array by subtracting mean and dividing by std along the given axis

    Args:
        data (numpy array) 

    Returns:
        standardized data (numpy array)

    Examples:
        >>> a=np.array([[0,1,2,3],[4,8,9,10]])
        >>> utils.standardize(a)
        array([[-1.34164079, -0.4472136 ,  0.4472136 ,  1.34164079],
               [-1.6464639 ,  0.10976426,  0.5488213 ,  0.98787834]])
    """
    means = np.mean(data, axis=axis, keepdims=True)
    stds = np.std(data, axis=axis, keepdims=True)
    return (data - means) / stds


def image_shape(filepath):
    """
    Get shape of a image file

    Args:
        filepath (str): path to the image file

    Returns:
        tuple of int
    """
    return np.array(Image.open(filepath)).shape


def random_perturbation(img, rescale_factor=1.2):
    """
    Perturb image

    Args:
        img (numpy array): image array
        rescale_factor (float): rescale factor for image enhancement (color, contrast, brightness)

    Returns:
        tuple of int for shape
    """
    im = Image.fromarray(img.astype(np.uint8))
    en_color = ImageEnhance.Color(im)
    im = en_color.enhance(random.uniform(1. / rescale_factor, rescale_factor))
    en_cont = ImageEnhance.Contrast(im)
    im = en_cont.enhance(random.uniform(1. / rescale_factor, rescale_factor))
    en_bright = ImageEnhance.Brightness(im)
    im = en_bright.enhance(random.uniform(1. / rescale_factor, rescale_factor))
    en_sharpness = ImageEnhance.Sharpness(im)
    im = en_sharpness.enhance(random.uniform(1. / rescale_factor, rescale_factor))
    return np.asarray(im)


def float2class(float_vals, min_val, max_val):
    """
    Round and clip float values to convert to categorical classes.

    Args:
        float_vals (list or array of floats): array of float values

    Examples:
        >>> float2class([0.1, 0.4, 0.6], 0, 1)
        [0, 0, 1]
    """
    return np.clip(np.round(float_vals), min_val, max_val)


def binary_stats(y_true, y_pred):
    """
    Return dictionary of statistics for binary classes

    Args:
        y_true (array-like): true class labels 
        y_pred (array-like): predicted class labels

    Returns:
        stats (dict): dict for statistics (confusion matrix, specificity, sensitivity, AUROC) 

    Note:
        if no positive cases exist in y_true, stats contains only specificity 
    """
    y_true = np.array(y_true)
    y_pred = np.array(y_pred)
    stats = {}
    if len(y_true[y_true == 1]) > 0:
        cm = confusion_matrix(y_true, float2class(y_pred, 0, 1))
        stats["confusion_matrix"] = cm
        stats["specificity"] = 1.*cm[0, 0] / (cm[0, 1] + cm[0, 0])
        stats["sensitivity"] = 1.*cm[1, 1] / (cm[1, 0] + cm[1, 1])
        stats["AUROC"] = roc_auc_score(y_true, y_pred)
    else:
        stats["specificity"] = 1.*len(y_true[y_true == y_pred]) / len(y_true)
    return stats


def categorical_stats(y_true, y_pred, weighted_kappa=True):
    """
    Return dictionary of statistics for categorical classes

    Args:
        y_true (array-like): true class labels 
        y_pred (array-like): predicted class labels
        weighted_kappa (bool): whether to return quadratic weighted kappa

    Returns:
        stats (dict): dict for statistics (confusion matrix, specificity, sensitivity, AUROC) 

    Note:
        if no positive cases exist in y_true, stats contains only specificity 
    """
    stats = {}
    cm = confusion_matrix(y_true, y_pred)
    stats["confusion_matrix"] = cm
    stats["accuracy"] = 1.*np.trace(cm) / np.sum(cm)
    if weighted_kappa:
        stats["weighted_kappa"] = cohen_kappa_score(
            y_true, y_pred, weights="quadratic")
    return stats


def save_imgs(imgs, dir_path):
    """
    Save images to a designated path

    Args:
        imgs (3d or 4d numpy array)
        dir_path (str): path to which images are saved

    Examples:
        >>> save_imgs(imgs, "results")
        1.png, 2.png, ..., N.png are saved
    """
    makedirs(dir_path)
    n_data = imgs.shape[0]
    for i in range(n_data):
        Image.fromarray((imgs[i, ...]).astype(np.uint8)).save(
            os.path.join(dir_path, "imgs_{}.png".format(i + 1)))


def run_once(func):

    def wrapper(*args, **kwargs):
        if not kwargs["phase"] in wrapper.phase_run:
            wrapper.phase_run.append(kwargs["phase"])
            return func(*args, **kwargs)

    wrapper.phase_run = []
    return wrapper


@run_once
def fundus_classification_check_data(data, dir_path, phase):
    filenames, imgs, labels = data
    save_imgs(imgs * 255, dir_path)


@run_once
def vessel_segmentation_check_data(data, dir_path, phase):
    imgs, masks = data
    save_imgs(imgs * 30 + 100, os.path.join(dir_path, "imgs"))
    save_imgs(masks * 255, os.path.join(dir_path, "masks"))


@run_once
def oct_classification_check_data(data, dir_path, phase):
    filenames, imgs, labels = data
    save_imgs(imgs * 255, dir_path)


@run_once
def oct_localization_check_data(data, dir_path):
    filenames, imgs, coords = data
    imgs = np.copy(imgs)
    coords = np.copy(coords)
    n, h, w = imgs.shape
    imgs = np.expand_dims(imgs, axis=-1)
    imgs = np.repeat(imgs, 3, axis=-1)
    coords[:, 1] *= w
#     coords[:, 3] *= w
    coords[:, 0] *= h
#     coords[:, 2] *= h
    for index in range(n):
        imgs[index, int(coords[index, 0]) - 3:int(coords[index, 0]) + 3,
             int(coords[index, 1]) - 3:int(coords[index, 1]) + 3, 0] = 1
#         imgs[index, int(coords[index, 2]) - 3:int(coords[index, 2]) + 3, int(coords[index, 3]) - 3:int(coords[index, 3]) + 3, 0] = 1

    save_imgs(imgs * 255, dir_path)


@run_once
def nicking_classification_check_data(data, dir_path, phase):
    filenames, imgs, labels = data
#     save_imgs(imgs * 50, dir_path)
    save_imgs(imgs * 255, dir_path)


def balanced_class_weights(labels):
    """
    Compute balanced class weights for individual samples given classification labels.

    Args:
        labels (array-like)

    Return:
        numpy array for individual sampling weights

    Examples:
        >>> balanced_class_weights([0,0,0,0,1,1,1,2])
        (array([0.15789474, 0.21052632, 0.63157895]), array([0.08333333, 0.08333333, 0.08333333, 0.08333333, 0.11111111,
       0.11111111, 0.11111111, 0.33333333]))

    """
    labels = np.array(labels)
    cw = class_weight.compute_class_weight(
        "balanced", np.unique(labels), labels)
    cw /= cw.sum()
    p = np.zeros(len(labels))
    for i, weight in enumerate(cw):
        p[labels == i] = weight
    p /= p.sum()
    return cw, p


def uniform_sample_weight(n_element):
    """
    Return uniform sample weight.

    Args:
        n_element (int)

    Return:
        numpy array for uniform sample weight

    Examples:
        >>> uniform_sample_weight(5)
        array([0.2, 0.2, 0.2, 0.2, 0.2])

    """
    return np.array([1. / n_element] * n_element)


def shell_command(command):
    """
    Execute a shell command in shell

    Args:
        command (str)

    Raises:
        AssertionError: command should be str

    """
    assert isinstance(command, str)
    pipes = Popen(command.split(), stdout=PIPE, stderr=PIPE)
    std_out, std_err = pipes.communicate()
    return std_out, std_err


def load_data_from_filepath_localization_v0(path_data, augment_func=None):
    """
    Load fundus image, vessel image, coords. Fundus image is standardized and vessel mask is normalized.

    Args:
        path_data (list): list of data (path of fundus image, path of vessel mask, coordinates)
        augment_func (function object): augmentation function
    """
    path_fundus, path_vessel, coords = path_data
    fundus_img = np.array(Image.open(path_fundus))
    vessel_img = np.array(Image.open(path_vessel))
    if augment_func:
        fundus_img, vessel_img, coords = augment_func(fundus_img, vessel_img, coords)
    
    # standardize fundus image
    fundus_img = standardize_fundus(fundus_img)
    
    # check whether vessel mask is normalized
    vessel_img = vessel_img.astype(np.float) / 255

    return fundus_img, vessel_img, coords




def load_img_from_filepath(filepath, normalization=None, denoise_func=None, augment_func=None):
    """
    Load an image designated by a path

    Args:
        filepath (str): full path of an image file
        normalization (str): normalization method for images
        denoise_func (function object): de-noising function
        augment_func (function object): augmentation function
    """
    img = np.array(Image.open(filepath))
    if denoise_func:
        img = denoise_func(img)
    if augment_func:
        img = augment_func(img)
    if normalization == "normalize":
        img = img / 255.0
    elif normalization == "z_score":
        img = standardize_fundus(img)
    return img


def load_img_mask_from_filepaths(filepaths, normalization=None, denoise_func=None, augment_func=None):
    """
    Load an image and a mask designated by a list of paths

    Args:
        filepaths (str): list of full path of an image file and a segmentation file
        normalization (str): normalization method
        denoise_func (function object): de-noising function
        augment_func (function object): augmentation function

    Returns:
        an image and a mask (numpy array)
    """
    img_filepath, mask_filepath = filepaths
    img = np.array(Image.open(img_filepath))
    mask = np.array(Image.open(mask_filepath))
    if denoise_func:
        img = denoise_func(img)
    if augment_func:
        img, mask = augment_func(img, mask)
    if normalization == "normalize":
        img = img / 255.0
    elif normalization == "z_score":
        means = np.zeros(3)
        stds = np.array([255.0, 255.0, 255.0])
        for i in range(3):
            if len(img[..., i][img[..., i] > 10]) > 1:
                means[i] = np.mean(img[..., i][img[..., i] > 10])
                std_val = np.std(img[..., i][img[..., i] > 10])
                if std_val > 0:
                    stds[i] = std_val
        img = (img - means) / stds

    if np.max(mask) != 1:
        if np.max(mask) == 255:
            mask[mask == 255] = 1
        else:
            raise ValueError("Mask should have intensity values of 255 or 1")

    return img, mask


def load_img_coords_from_filepath(filepath, coords, normalization=None, denoise_func=None, augment_func=None):
    """
    Load an image designated by a path and a coordinate

    Args:
        filepath (str): full path of an image file
        coords (numpy): 1d array for coords
        normalization (str): normalization method for images
        denoise_func (function object): de-noising function
        augment_func (function object): augmentation function


    """
    img = np.array(Image.open(filepath))
    if denoise_func:
        img = denoise_func(img)
    if augment_func:
        img, coords = augment_func(img, coords)
    if normalization == "normalize":
        img = img / 255.0
    elif normalization == "z_score":
        means = np.zeros(3)
        stds = np.array([255.0, 255.0, 255.0])
        for i in range(3):
            if len(img[..., i][img[..., i] > 10]) > 1:
                means[i] = np.mean(img[..., i][img[..., i] > 10])
                std_val = np.std(img[..., i][img[..., i] > 10])
                if std_val > 0:
                    stds[i] = std_val
        img = (img - means) / stds

    return img, coords


def resize_img(img, h_target, w_target, method="bicubic"):
    """
    Wrapper for scipy imresize.
    """
    return imresize(img, (h_target, w_target), method)


def pad_imgs(imgs, img_size, is_batch=True):
    """
    Pad a batch of images with the shape of (N, h, w, d) or (N, h, w) if is_batch=True,
        an image with the shape of (h, w, d) or (h, w), otherwise

    Args:
        imgs (numpy array): a batch of images with the shape of (N, h, w, d) or (N, h, w)
        img_size (tuple): (target_h, target_w)

    Returns:
        padded images (numpy array)

    """
    if is_batch:
        assert len(imgs.shape) == 3 or len(imgs.shape) == 4
        img_h, img_w = imgs.shape[1], imgs.shape[2]
        target_h, target_w = img_size[0], img_size[1]
        if len(imgs.shape) == 4:
            d = imgs.shape[3]
            padded = np.zeros((imgs.shape[0], target_h, target_w, d))
        elif len(imgs.shape) == 3:
            padded = np.zeros((imgs.shape[0], img_size[0], img_size[1]))
        padded[:, (target_h - img_h) // 2:(target_h - img_h) // 2 + img_h,
               (target_w - img_w) // 2:(target_w - img_w) // 2 + img_w, ...] = imgs
    else:
        assert len(imgs.shape) == 2 or len(imgs.shape) == 3
        img_h, img_w = imgs.shape[0], imgs.shape[1]
        target_h, target_w = img_size[0], img_size[1]
        if len(imgs.shape) == 3:
            padded = np.zeros((target_h, target_w, imgs.shape[2]))
        elif len(imgs.shape) == 2:
            padded = np.zeros((img_size[0], img_size[1]))
        padded[(target_h - img_h) // 2:(target_h - img_h) // 2 + img_h,
               (target_w - img_w) // 2:(target_w - img_w) // 2 + img_w, ...] = imgs
    return padded


def available_gpu(idle_time=300):
    """
    Check gpu availability by nvidia-smi.
    Judge a gpu is idle when no memory is consumed for idle_time seconds.

    Args:
        idle_time (int): if a gpu is idle more than idel_time seconds, it is judged as idle.

    Returns:
        Available gpu index (list of str)

    """
    list_idle = []
    for _ in range(2):
        std_out, _ = shell_command("nvidia-smi")
        lines = std_out.decode("utf-8").split("\n")
        existing, occupied = [], []
        occupied_line = False
        for index, line in enumerate(lines):
            if "Processes" in line:
                occupied_line = True
            else:
                if "MiB" in line:
                    if occupied_line:
                        occupied.append(line.split()[1])
                    else:
                        existing.append(lines[index - 1].split()[1])
        idle = set(existing) - set(occupied)
        if len(idle) == 0:
            return None
        else:
            list_idle.append(idle)
            if len(list_idle) == 2:
                return list(list_idle[0].intersection(list_idle[1]))
            # wait for idle_time seconds and check if the gpu is still idle
            time.sleep(idle_time)

########################################################
########################################################
########### TASK SPECIFIC UTILITY FUNCTIONS ############
########################################################
########################################################


def merge_dr_label_open_data(list_df):
    """
    Merge DR labels for multiple open data

    Args:
        list_df: a list of dataframes (pandas)

    Return:
        df_concat: a concatenated dataframe (pandas)

    Raises:
        AssertionError: 1. list_df should have at least 1 element
                        2. duplicate fid(s) exist
    """
    assert len(list_df) > 0
    list_df_processed = []
    for df in list_df:
        if "id_code" in df.columns:  # APTOS
            df["id"] = df["id_code"]
            df["label"] = df["diagnosis"]
        elif "Image name" in df.columns:  # IDRiD
            df["id"] = df["Image name"]
            df["label"] = df["Retinopathy grade"]
        elif "image" in df.columns:  # kaggle
            df["id"] = df["image"]
            df["label"] = df["level"]
        elif "image_id" in df.columns:  # messidor
            df["id"] = df["image_id"].map(lambda x: drop_extension(x))
            df["label"] = df["adjudicated_dr_grade"]
        df.dropna(subset=["label"], inplace=True)
        list_df_processed.append(df[["id", "label"]])
    if len(list_df_processed) > 1:
        df_concat = pd.concat(list_df_processed, ignore_index=True)
    else:
        df_concat = list_df_processed[0]
    assert len(np.unique(df_concat["id"])) == len(
        df_concat)  # no duplicate in fid
    df_concat.set_index('id', inplace=True)
    return df_concat


def assign_labels_oct(fpaths):
    """
    Args:
        fpaths (list of str): file paths of the input images

    Returns:
        labels (2d numpy arrray): one-hot vector labels
    """
    n_classes = 4
    labels = np.zeros((len(fpaths), n_classes))
    for index, fpath in enumerate(fpaths):
        fname = os.path.basename(fpath)
        if "NORMAL" in fname:
            labels[index, 0] = 1
        elif "DRUSEN" in fname:
            labels[index, 1] = 1
        elif "CNV" in fname:
            labels[index, 2] = 1
        elif "DME" in fname:
            labels[index, 3] = 1
        else:
            raise ValueError("fname {} does not specify class".format(fname))

    return labels


def fundus_denoise(img):
    """
    Remove low values & erase time tag

    Args: 
        img (numpy array)

    Returns:
        denoised image in numpy array

    """
    img[img < 10] = 0  # remove low values
    img[:80, ...] = 0  # remove time tag
    return img


def standardize_fundus(data):
    """
    Standardize numpy array by subtracting mean and dividing by std.
    mean and std are computed for pixels with intensity above 10.

    Args:
        data (numpy array) 

    Returns:
        standardized data (numpy array)
    """
    means = np.zeros(3)
    stds = np.array([255.0, 255.0, 255.0])
    for i in range(3):
        if len(data[..., i][data[..., i] > 10]) > 1:
            means[i] = np.mean(data[..., i][data[..., i] > 10])
            std_val = np.std(data[..., i][data[..., i] > 10])
            if std_val > 0:
                stds[i] = std_val
    return (data - means) / stds


def fundus_localization_augment(fundus_img, vessel_img, coords, albumentation_aug_prob = 0.1):
    """
    Color, contrast, brightness perturbation, horizontal flip, affine transform (rotation, resize), 
    coarse dropout, blur, gauss noise, ISONoise, jpeg compression, rgb shift, random gamma,
    random sunflare, downsample

    Args: 
        fundus_img (numpy array, shape: (h,w,d))
        vessel_img (numpy array, shape: (h,w,d))
        coords (numpy array, shape: (2,))
        albumentation_aug_prob (float)

    Returns:
        augmented image in numpy array

    """
    h, w, d = fundus_img.shape

    shift_y, shift_x = np.array(fundus_img.shape[:2]) / 2.
    shift_to_ori = AffineTransform(translation=[-shift_x, -shift_y])
    shift_to_img_center = AffineTransform(translation=[shift_x, shift_y])
    r_angle = random.randint(0, 359)
    scale_factor = 1.3
    scale = random.uniform(1. / scale_factor, scale_factor)
    tform = AffineTransform(scale=(scale, scale), rotation=np.deg2rad(r_angle))
    aug_fundus = warp(fundus_img, (shift_to_ori + (tform + shift_to_img_center)
                             ).inverse, output_shape=(fundus_img.shape[0], fundus_img.shape[1]))
    aug_vessel = warp(vessel_img, (shift_to_ori + (tform + shift_to_img_center)
                             ).inverse, output_shape=(vessel_img.shape[0], vessel_img.shape[1]))
    aug_coords = rotate_scale_pt((0.5*h, 0.5*w), coords, r_angle, scale)
    translation = {}
    # # translation until legal coordinates
    # is_inside_img = False
    # while not is_inside_img:
    #     translation["h"] = random.uniform(-h/2, h/2)
    #     translation["w"] = random.uniform(-w/2, w/2)
    #     is_inside_img = inside_box((aug_coords[0] + translation["h"], aug_coords[1] + translation["w"]), (h,w))
    # translation at random
    translation["h"] = random.uniform(-h/2, h/2)
    translation["w"] = random.uniform(-w/2, w/2)
    is_inside_img = inside_box((aug_coords[0] + translation["h"], aug_coords[1] + translation["w"]), (h,w))
    
    aug_coords = aug_coords + np.array([translation["h"], translation["w"]])
    translation_transform = AffineTransform(translation=(translation["w"], translation["h"]))
    aug_fundus = warp(aug_fundus, translation_transform.inverse, output_shape=(fundus_img.shape[0], fundus_img.shape[1]))
    aug_vessel = warp(aug_vessel, translation_transform.inverse, output_shape=(vessel_img.shape[0], vessel_img.shape[1]))
    aug_fundus = (aug_fundus * 255).astype(np.uint8)
    aug_vessel = (aug_vessel * 255).astype(np.uint8)

    # CoarseDropout
    if random.uniform(0, 1) < albumentation_aug_prob:
        aug_fundus = transforms.CoarseDropout(max_holes=5, max_height=aug_fundus.shape[0] // 10, max_width=aug_fundus.shape[0] //
                                       10, min_holes=None, min_height=None, min_width=None, p=1)(image=aug_fundus)["image"].astype(np.uint8)
        aug_vessel = transforms.CoarseDropout(max_holes=5, max_height=aug_vessel.shape[0] // 10, max_width=aug_vessel.shape[0] //
                                       10, min_holes=None, min_height=None, min_width=None, p=1)(image=aug_vessel)["image"].astype(np.uint8)


    # blur
    blur_limit = 10
    if random.uniform(0, 1) < albumentation_aug_prob:
        aug_fundus = transforms.Blur(blur_limit=blur_limit, p=1)(
            image=aug_fundus)["image"].astype(np.uint8)

    # GaussNoise
    GaussNoise_val_limit = 80
    if random.uniform(0, 1) < albumentation_aug_prob:
        aug_fundus = transforms.GaussNoise(var_limit=GaussNoise_val_limit, p=1)(
            image=aug_fundus)["image"].astype(np.uint8)

    # ISONoise
    ISONoise_color_shift_max = 0.1
    if random.uniform(0, 1) < albumentation_aug_prob:
        aug_fundus = transforms.ISONoise(color_shift=(0.01, ISONoise_color_shift_max), p=1)(
            image=aug_fundus)["image"].astype(np.uint8)

    # JpegCompression
    JpegCompression_quality_lower = 60
    if random.uniform(0, 1) < albumentation_aug_prob:
        aug_fundus = transforms.JpegCompression(quality_lower=JpegCompression_quality_lower, quality_upper=100, p=1)(
            image=aug_fundus)["image"].astype(np.uint8)

    # RGBShift
    RGBShift_val = 40
    if random.uniform(0, 1) < albumentation_aug_prob:
        aug_fundus = transforms.RGBShift(r_shift_limit=RGBShift_val, g_shift_limit=RGBShift_val,
                                      b_shift_limit=RGBShift_val, p=1)(image=aug_fundus)["image"].astype(np.uint8)

    # RandomGamma
    RandomGamma_min = 50
    RandomGamma_max = 150
    if random.uniform(0, 1) < albumentation_aug_prob:
        aug_fundus = transforms.RandomGamma(gamma_limit=(
            RandomGamma_min, RandomGamma_max), p=1)(image=aug_fundus)["image"].astype(np.uint8)

    # RandomSunFlare
    RandomSunFlare_radius = int(random.uniform(256, 512))
    RandomSunFlare_draw = random.uniform(0, 1)
    if RandomSunFlare_draw < 0.25:
        flare_roi = (0, 0, 0.25, 0.25)
    elif RandomSunFlare_draw < 0.5:
        flare_roi = (0.75, 0, 1, 0.25)
    elif RandomSunFlare_draw < 0.75:
        flare_roi = (0, 0.75, 0.25, 1)
    else:
        flare_roi = (0.75, 0.75, 1, 1)
    if random.uniform(0, 1) < albumentation_aug_prob:
        aug_fundus = transforms.RandomSunFlare(flare_roi=flare_roi, src_radius=RandomSunFlare_radius,
                                            num_flare_circles_lower=0, num_flare_circles_upper=1, p=1)(image=aug_fundus)["image"].astype(np.uint8)

    # downsample
    if random.uniform(0, 1) < albumentation_aug_prob:
        downsize_factor = random.uniform(1, 2)
        aug_fundus = imresize(aug_fundus, (int(h / downsize_factor),
                                     int(w / downsize_factor)), interp="nearest")
        aug_fundus = imresize(aug_fundus, (h, w), interp="nearest").astype(np.uint8)

    return aug_fundus, aug_vessel, aug_coords

def fundus_classification_augment(img):
    """
    Color, contrast, brightness perturbation, horizontal flip, affine transform (rotation, resize, shear), 
    grid distortion, coarse dropout, blur, gauss noise, ISONoise, jpeg compression, rgb shift, random gamma,
    random sunflare, elastic transform, downsample

    Args: 
        img (numpy array)

    Returns:
        augmented image in numpy array
    """
    h, w, d = img.shape

    aug_img = random_perturbation(img, 1.5)
    shift_y, shift_x = np.array(aug_img.shape[:2]) / 2.
    shift_to_ori = AffineTransform(translation=[-shift_x, -shift_y])
    shift_to_img_center = AffineTransform(translation=[shift_x, shift_y])
    r_angle = random.randint(0, 359)
    scale_factor = 1.3
    scale = random.uniform(1. / scale_factor, scale_factor)
    shear = random.uniform(-10, 10)
    tform = AffineTransform(scale=(scale, scale), rotation=np.deg2rad(r_angle), shear=np.deg2rad(shear))
    aug_img = warp(aug_img, (shift_to_ori + (tform + shift_to_img_center)
                             ).inverse, output_shape=(aug_img.shape[0], aug_img.shape[1]))
    aug_img = (aug_img * 255).astype(np.uint8)

    PROB_AUG = 0.1
    # GridDistortion
    if random.uniform(0, 1) < PROB_AUG:
        aug_img = transforms.GridDistortion(num_steps=5, distort_limit=0.5, value=None,
                                        mask_value=None, always_apply=True, p=1)(image=aug_img)["image"].astype(np.uint8)

    # CoarseDropout
    if random.uniform(0, 1) < PROB_AUG:
        aug_img = transforms.CoarseDropout(max_holes=5, max_height=aug_img.shape[0] // 10, max_width=aug_img.shape[0] //
                                       10, min_holes=None, min_height=None, min_width=None, p=1)(image=aug_img)["image"].astype(np.uint8)

    # blur
    blur_limit = 10
    if random.uniform(0, 1) < PROB_AUG:
        aug_img = transforms.Blur(blur_limit=blur_limit, p=1)(
            image=aug_img)["image"].astype(np.uint8)

    # GaussNoise
    GaussNoise_val_limit = 80
    if random.uniform(0, 1) < PROB_AUG:
        aug_img = transforms.GaussNoise(var_limit=GaussNoise_val_limit, p=1)(
            image=aug_img)["image"].astype(np.uint8)

    # ISONoise
    ISONoise_color_shift_max = 0.1
    if random.uniform(0, 1) < PROB_AUG:
        aug_img = transforms.ISONoise(color_shift=(0.01, ISONoise_color_shift_max), p=1)(
            image=aug_img)["image"].astype(np.uint8)

    # JpegCompression
    JpegCompression_quality_lower = 60
    if random.uniform(0, 1) < PROB_AUG:
        aug_img = transforms.JpegCompression(quality_lower=JpegCompression_quality_lower, quality_upper=100, p=1)(
            image=aug_img)["image"].astype(np.uint8)

    # RGBShift
    RGBShift_val = 40
    if random.uniform(0, 1) < PROB_AUG:
        aug_img = transforms.RGBShift(r_shift_limit=RGBShift_val, g_shift_limit=RGBShift_val,
                                      b_shift_limit=RGBShift_val, p=1)(image=aug_img)["image"].astype(np.uint8)

    # RandomGamma
    RandomGamma_min = 50
    RandomGamma_max = 150
    if random.uniform(0, 1) < PROB_AUG:
        aug_img = transforms.RandomGamma(gamma_limit=(
            RandomGamma_min, RandomGamma_max), p=1)(image=aug_img)["image"].astype(np.uint8)

    # RandomSunFlare
    RandomSunFlare_radius = int(random.uniform(256, 512))
    RandomSunFlare_draw = random.uniform(0, 1)
    if RandomSunFlare_draw < 0.25:
        flare_roi = (0, 0, 0.25, 0.25)
    elif RandomSunFlare_draw < 0.5:
        flare_roi = (0.75, 0, 1, 0.25)
    elif RandomSunFlare_draw < 0.75:
        flare_roi = (0, 0.75, 0.25, 1)
    else:
        flare_roi = (0.75, 0.75, 1, 1)
    if random.uniform(0, 1) < PROB_AUG:
        aug_img = transforms.RandomSunFlare(flare_roi=flare_roi, src_radius=RandomSunFlare_radius,
                                            num_flare_circles_lower=0, num_flare_circles_upper=1, p=1)(image=aug_img)["image"].astype(np.uint8)

    # ElasticTransform
    ElasticTransform_alpha = int(random.uniform(512, 1024))
    ElasticTransform_sigma = ElasticTransform_alpha * \
        random.uniform(0.03, 0.05)
    if random.uniform(0, 1) < PROB_AUG:
        aug_img = transforms.ElasticTransform(alpha=ElasticTransform_alpha, sigma=ElasticTransform_sigma, alpha_affine=0, p=1)(
            image=aug_img)["image"].astype(np.uint8)

    # downsample
    if random.uniform(0, 1) < PROB_AUG:
        downsize_factor = random.uniform(1, 2)
        aug_img = imresize(aug_img, (int(h / downsize_factor),
                                     int(w / downsize_factor)), interp="nearest")
        aug_img = imresize(aug_img, (h, w), interp="nearest").astype(np.uint8)

    return aug_img


def fundus_classification_augment_v0(img, scale_factor=1.15, rotation_from_angle=0, rotation_to_angle=360):
    """
    Color, contrast, brightness perturbation, horizontal flip, affine transform (rotation, resize)

    Args: 
        img (numpy array)
        scale_factor (float): image is scaled from 1. / scale_factor to scale_factor
        rotation_from_angle (float): angle from which an image is rotated
        rotation_to_angle (float): angle to which an image is rotated

    Returns:
        augmented image in numpy array

    """
    img = np.copy(img)
    # random color, contrast, brightness perturbation
    img = random_perturbation(img)
    if random.getrandbits(1):
        img = img[:, ::-1, :]  # random horizontal flip
    # affine transform (scale, rotation)
    shift_y, shift_x = np.array(img.shape[:2]) / 2.
    shift_to_ori = AffineTransform(translation=[-shift_x, -shift_y])
    shift_to_img_center = AffineTransform(translation=[shift_x, shift_y])
    r_angle = np.random.uniform(rotation_from_angle, rotation_to_angle)
    scale = random.uniform(1. / scale_factor, scale_factor)
    tform = AffineTransform(scale=(scale, scale), rotation=np.deg2rad(r_angle))
    img = warp(img, (shift_to_ori + (tform + shift_to_img_center)
                     ).inverse, output_shape=(img.shape[0], img.shape[1]))
    img *= 255  # revert to 0-255 range
    return img


def nicking_classification_augment(img, scale_factor=1.15, rotation_from_angle=0, rotation_to_angle=360, translation_ratio=0.1):
    """
    Color, contrast, brightness perturbation, horizontal flip, affine transform (rotation, resize, translation)
    and Resize the img to 128x128

    Args: 
        img (numpy array)
        scale_factor (float): image is scaled from 1. / scale_factor to scale_factor
        rotation_from_angle (float): angle from which an image is rotated
        rotation_to_angle (float): angle to which an image is rotated

    Returns:
        augmented image in numpy array

    """
    img = np.copy(img)
    # random color, contrast, brightness perturbation
    img = random_perturbation(img)
    if random.getrandbits(1):
        img = img[:, ::-1, :]  # random horizontal flip
    # affine transform (scale, rotation)
    shift_y, shift_x = np.array(img.shape[:2]) / 2.
    shift_to_ori = AffineTransform(translation=[-shift_x, -shift_y])
    shift_to_img_center = AffineTransform(translation=[shift_x, shift_y])
    r_angle = np.random.uniform(rotation_from_angle, rotation_to_angle)
    scale = random.uniform(1. / scale_factor, scale_factor)
    h_trans_margin = translation_ratio * img.shape[0]
    w_trans_margin = translation_ratio * img.shape[1]
    h_translation = int(random.uniform(-h_trans_margin, h_trans_margin))
    w_translation = int(random.uniform(-w_trans_margin, w_trans_margin))
    tform = AffineTransform(scale=(scale, scale), rotation=np.deg2rad(
        r_angle), translation=(w_translation, h_translation))
    img = warp(img, (shift_to_ori + (tform + shift_to_img_center)
                     ).inverse, output_shape=(img.shape[0], img.shape[1]))
    img *= 255  # revert to 0-255 range
    img = imresize(img, (128, 128), 'bicubic')  # resize img as size varies
    return img


def oct_classification_augment(img, scale_factor=1.15, rotation_from_angle=0, rotation_to_angle=360, shear_degree=10):
    """
    Color, contrast, brightness perturbation, horizontal flip, affine transform (share, rotation, resize)

    Args: 
        img (numpy array)
        scale_factor (float): image is scaled from 1. / scale_factor to scale_factor
        rotation_from_angle (float): angle from which an image is rotated
        rotation_to_angle (float): angle to which an image is rotated

    Returns:
        augmented image in numpy array

    """
    img = np.copy(img)
    # random color, contrast, brightness perturbation
    img = random_perturbation(img)
    if random.getrandbits(1):
        img = img[:, ::-1]  # random horizontal flip
    # affine transform (shear, scale, rotation)
    shift_y, shift_x = np.array(img.shape[:2]) / 2.
    shift_to_ori = AffineTransform(translation=[-shift_x, -shift_y])
    shift_to_img_center = AffineTransform(translation=[shift_x, shift_y])
    r_angle = np.random.uniform(rotation_from_angle, rotation_to_angle)
    scale = random.uniform(1. / scale_factor, scale_factor)
    shear = random.uniform(-shear_degree, shear_degree)
    tform = AffineTransform(scale=(scale, scale), rotation=np.deg2rad(
        r_angle), shear=np.deg2rad(shear))
    img = warp(img, (shift_to_ori + (tform + shift_to_img_center)
                     ).inverse, output_shape=(img.shape[0], img.shape[1]))
    # add noise
    noisy_img = random_noise(img)
    noisy_img *= 255
    return noisy_img


def oct_localization_augment(img, coords, scale_factor=1.3, rotation_from_angle=-15, rotation_to_angle=15):
    """
    Color, contrast, brightness perturbation, horizontal flip, affine transform
    CAVEATE: assume oct image size is 512x1024 (h,w)

    Args: 
        img (numpy array)
        coords: coords for (ly,lx,ry,rx)
        scale_factor (float): image is scaled from 1. / scale_factor to scale_factor
        rotation_from_angle (float): angle from which an image is rotated
        rotation_to_angle (float): angle to which an image is rotated

    Returns:
        augmented image in numpy array
        converted coords in numpy array (ly,lx,ry,rx)

    """
    h, w = 512, 1024
    img = np.copy(img)
    converted_coords = np.copy(coords)
    converted_coords[0] *= h
    converted_coords[2] *= h
    converted_coords[1] *= w
    converted_coords[3] *= w
    # random color, contrast, brightness perturbation
    img = random_perturbation(img)
    if random.getrandbits(1):
        img = img[:, ::-1]  # random horizontal flip
        converted_coords[1] = w - converted_coords[1]  # lx
        converted_coords[3] = w - converted_coords[3]  # rx

    # affine transform (scale, rotation)
    shift_y, shift_x = np.array(img.shape[:2]) / 2.
    shift_to_ori = AffineTransform(translation=[-shift_x, -shift_y])
    shift_to_img_center = AffineTransform(translation=[shift_x, shift_y])
    r_angle = np.random.uniform(rotation_from_angle, rotation_to_angle)
    scale = random.uniform(1. / scale_factor, scale_factor)
    tform = AffineTransform(scale=(scale, scale), rotation=np.deg2rad(r_angle))
    img = warp(img, (shift_to_ori + (tform + shift_to_img_center)
                     ).inverse, output_shape=(img.shape[0], img.shape[1]))

    # adjust coords
    converted_coords[0] = scale * (converted_coords[0] - shift_y)
    converted_coords[1] = scale * (converted_coords[1] - shift_x)
    converted_coords[2] = scale * (converted_coords[2] - shift_y)
    converted_coords[3] = scale * (converted_coords[3] - shift_x)
    converted_coords[:2] = rotate_pt(
        (0, 0), (converted_coords[0], converted_coords[1]), r_angle)
    converted_coords[2:] = rotate_pt(
        (0, 0), (converted_coords[2], converted_coords[3]), r_angle)
    converted_coords[0] += shift_y
    converted_coords[1] += shift_x
    converted_coords[2] += shift_y
    converted_coords[3] += shift_x
    converted_coords[0] /= h
    converted_coords[2] /= h
    converted_coords[1] /= w
    converted_coords[3] /= w

    # add noise
    noisy_img = random_noise(img)
    noisy_img *= 255
    return noisy_img, converted_coords


def vessel_segmentation_augment(img, mask, scale_factor=1.3, rotation_from_angle=0, rotation_to_angle=360, shear_degree=10):
    """
    Horizontal flip, affine transform

    Args: 
        img (numpy array)
        vessel (numpy array)
        scale_factor (float): image is scaled from 1. / scale_factor to scale_factor
        rotation_from_angle (float): angle from which an image is rotated
        rotation_to_angle (float): angle to which an image is rotated

    Returns:
        augmented image in numpy array

    """
    img = np.copy(img)
    mask = np.copy(mask)
    if random.getrandbits(1):
        img = img[:, ::-1, :]  # random horizontal flip
        mask = mask[:, ::-1]
    # affine transform (scale, rotation)
    shift_y, shift_x = np.array(img.shape[:2]) / 2.
    shift_to_ori = AffineTransform(translation=[-shift_x, -shift_y])
    shift_to_img_center = AffineTransform(translation=[shift_x, shift_y])
    r_angle = np.random.uniform(rotation_from_angle, rotation_to_angle)
    scale = random.uniform(1. / scale_factor, scale_factor)
    shear = random.uniform(-shear_degree, shear_degree)
    tform = AffineTransform(scale=(scale, scale), rotation=np.deg2rad(
        r_angle), shear=np.deg2rad(shear))
    img = warp(img, (shift_to_ori + (tform + shift_to_img_center)
                     ).inverse, output_shape=(img.shape[0], img.shape[1]))
    img *= 255  # revert back to 8 bit image
    mask = warp(mask, (shift_to_ori + (tform + shift_to_img_center)
                       ).inverse, output_shape=(mask.shape[0], mask.shape[1]))
    mask = np.round(mask)
    return img, mask


def fundus_localization_processing_func_train(data):
    return load_data_from_filepath_localization_v0(data, fundus_localization_augment)

def fundus_localization_processing_func_val(data):
    return load_data_from_filepath_localization_v0(data, None)


def fundus_classification_processing_func_train_DeepDRiD(data):
    return load_img_from_filepath(data[0], "normalize", None, fundus_classification_augment), data[1], \
        load_img_from_filepath(data[2], "normalize", None, fundus_classification_augment), data[3], \
        load_img_from_filepath(
            data[4], "normalize", None, fundus_classification_augment), data[5]

def fundus_classification_processing_func_val_DeepDRiD(data):
    return load_img_from_filepath(data[0], "normalize", None), data[1]


# obsolte
def fundus_classification_processing_func_train(data):
    return data[0], load_img_from_filepath(data[0], "normalize", fundus_denoise, fundus_classification_augment_v0), data[1]

# obsolte
def fundus_classification_processing_func_val(data):
    return data[0], load_img_from_filepath(data[0], "normalize", fundus_denoise), data[1]

# obsolte
def vessel_segmentation_processing_func_train(filepaths):
    return load_img_mask_from_filepaths(filepaths, "z_score", augment_func=vessel_segmentation_augment)

# obsolte
def vessel_segmentation_processing_func_val(filepaths):
    return load_img_mask_from_filepaths(filepaths, "z_score")

# obsolte
def oct_classification_processing_func_train(data):
    return data[0], load_img_from_filepath(data[0], "normalize", augment_func=nicking_classification_augment), data[1]

# obsolte
def oct_classification_processing_func_val(data):
    return data[0], load_img_from_filepath(data[0], "normalize"), data[1]

# obsolte
def oct_localization_processing_func_train(data):
    img, coord = load_img_coords_from_filepath(
        data[0], data[1], "normalize", augment_func=oct_localization_augment)
    return data[0], img, coord

# obsolte
def oct_localization_processing_func_val(data):
    img, coord = load_img_coords_from_filepath(data[0], data[1], "normalize")
    return data[0], img, coord

# obsolte
def nicking_classification_processing_func_train(data):
    return data[0], load_img_from_filepath(data[0], "z_score", augment_func=nicking_classification_augment), data[1]
#     return data[0], load_img_from_filepath(data[0], "normalize", augment_func=nicking_classification_augment), data[1]

# obsolte
def nicking_classification_processing_func_val(data):
    return data[0], load_img_from_filepath(data[0], "z_score"), data[1]
#     return data[0], load_img_from_filepath(data[0], "normalize"), data[1]


def crop_fundus_img(img, red_threshold=20):
    """
    Crop a fundus image to remove the black background.
    Following images are not cropped
        - Pixels have red channel values < red_threshold
        - Pixels in central area are dominantly < red_threshold
        - The largest blob with red channel > red_threshold has a height or width less than 100

    Args:
        img (3d numpy array): fundus image

    Returns:
        resized img (3d numpy array)

    """

    # read the image and resize
    h_ori, w_ori, _ = np.shape(img)
    roi_check_len = h_ori // 5

    # Find Connected Components with intensity above the threshold
    blobs_labels, n_blobs = measure.label(
        img[:, :, 0] > red_threshold, return_num=True)
    if n_blobs == 0:
        raise ValueError("crop failed [no blob found]")

    # Find the Index of Connected Components of the Fundus Area (the central area)
    majority_vote = np.argmax(np.bincount(blobs_labels[h_ori // 2 - roi_check_len // 2:h_ori // 2 + roi_check_len // 2,
                                                       w_ori // 2 - roi_check_len // 2:w_ori // 2 + roi_check_len // 2].flatten()))
    if majority_vote == 0:
        raise ValueError("crop failed [invisible areas (intensity in red channel less than " + str(red_threshold) + ") are dominant in the center]")

    row_inds, col_inds = np.where(blobs_labels == majority_vote)
    row_max = np.max(row_inds)
    row_min = np.min(row_inds)
    col_max = np.max(col_inds)
    col_min = np.min(col_inds)
    if row_max - row_min < 100 or col_max - col_min < 100:
        for i in range(1, n_blobs + 1):
            print(len(blobs_labels[blobs_labels == i]))
        raise ValueError("crop failed [too small areas]")

    # crop the image
    crop_img = img[row_min:row_max, col_min:col_max]
    max_len = max(crop_img.shape[0], crop_img.shape[1])
    img_h, img_w, _ = crop_img.shape
    padded = np.zeros((max_len, max_len, 3), dtype=np.uint8)
    padded[(max_len - img_h) // 2:(max_len - img_h) // 2 + img_h,
           (max_len - img_w) // 2:(max_len - img_w) // 2 + img_w, ...] = crop_img
    return padded


def network_input_fundus(filepath):
    """
    Prepare a network input for a file

    Args:
        filepath (str): path to a file

    Returns:
        cropped and resized and normalized numpy array for a fundus image

    """
    input_size = (512, 512)  # (h,w)
    img = np.array(Image.open(filepath))
    cropped_img = crop_fundus_img(img)
    resized_img = resize_img(cropped_img, input_size[0], input_size[1])
    normalized_resized_img = normalize(resized_img)
    return np.expand_dims(normalized_resized_img, axis=0)


def network_input_nicking(filepath):
    """
    Prepare a network input for a file

    Args:
        filepath (str): path to a file

    Returns:
        cropped and resized and normalized numpy array for a fundus image

    """
    input_size = (128, 128)  # (h,w)
    img = np.array(Image.open(filepath))
    resized_img = resize_img(img, input_size[0], input_size[1])
    # compute z-score
    means = np.zeros(3)
    stds = np.array([255.0, 255.0, 255.0])
    for i in range(3):
        if len(resized_img[..., i][resized_img[..., i] > 10]) > 1:
            means[i] = np.mean(resized_img[..., i][resized_img[..., i] > 10])
            std_val = np.std(resized_img[..., i][resized_img[..., i] > 10])
            if std_val > 0:
                stds[i] = std_val
        normalized_resized_img = (resized_img - means) / stds
    return np.expand_dims(normalized_resized_img, axis=0)


def save_heatmap_funuds_img(img, activation, outdir, filepath):
    """
    Save heatmap for fundus 

    Args:
        img (numpy array): a numpy array for an image (h,w,d)
        activation (numpy arrray): a numpy array for an activation map
        outdir (str): output directory
        filepath (str): path to the original file 

    """
    makedirs(outdir)
    out_fn = os.path.basename(filepath) if filepath else ""
    Image.fromarray(img.astype(np.uint8)).save(
        os.path.join(outdir, out_fn + "original.png"))
    h_target, w_target, _ = img.shape
    heatmap = imresize(activation, (h_target, w_target), 'bilinear')
    heatmap_pil_img = Image.fromarray(heatmap)
    heatmap_pil_img = heatmap_pil_img.filter(ImageFilter.GaussianBlur(16))
    heatmap_blurred = np.asarray(heatmap_pil_img)
    cm = cm.get_cmap("Spectral")
    heatmap_blurred = np.uint8(cm.jet(heatmap_blurred)[..., :3] * 255)
    overlapped = (img * 0.7 + heatmap_blurred * 0.3).astype(np.uint8)
    Image.fromarray(overlapped).save(os.path.join(
        outdir, out_fn + "activation_overlapped.png"))


########################################################
########################################################
################## METRIC FUNCTIONS ####################
########################################################
########################################################
def dice(arr1, arr2):
    """
    Compute dice coefficient using scipy package

    Args:
        arr1, arr2: two numpy arrays

    Returns:
        dice coefficient (float)

    """
    arr1_bool = arr1.astype(np.bool)
    arr2_bool = arr2.astype(np.bool)
    intersection = np.count_nonzero(arr1_bool & arr2_bool)
    size1 = np.count_nonzero(arr1)
    size2 = np.count_nonzero(arr2)
    try:
        dc = 2. * intersection / float(size1 + size2)
    except ZeroDivisionError:
        dc = 0.0

    return dc
