import argparse
import os
import utils
import time
import numpy as np
from PIL import Image

def vessel_segmentation(imgdir, outdir):
    # set paths
    vessel_model_dir = "model/vessel"
    
    # load the models and corresponding weights
    vessel_model = utils.load_network(vessel_model_dir)
    
    for filepath in utils.all_files_under(imgdir):
        # load an img (tensor shape of [1,h,w,3])
        if "T" not in os.path.basename(filepath):
            continue
        img = utils.imagefiles2arrs([filepath])
        resized_img, y_offset, x_offset, cropped_w = utils.resize_img(img, 640, 640)

        # run inference
        vessel = vessel_model.predict(utils.normalize(resized_img, "vessel_segmentation"), batch_size=1)
        Image.fromarray((vessel[0,...,0] * 255).astype(np.uint8)).save(os.path.join(outdir, os.path.basename(filepath)))


# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--imgdir',
    type=str,
    required=True
    )
parser.add_argument(
    '--outdir',
    type=str,
    required=True
    )
parser.add_argument(
    '--gpu_index',
    type=str,
    required=True
    )
FLAGS, _ = parser.parse_known_args()

# training settings 
os.environ['CUDA_VISIBLE_DEVICES'] = FLAGS.gpu_index

# localization
os.makedirs(FLAGS.outdir, exist_ok=True)
vessel_segmentation(FLAGS.imgdir, FLAGS.outdir)
