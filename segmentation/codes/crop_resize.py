# crop center to be width/height = 1.15
import argparse

from PIL import Image

import numpy as np
import os
import multiprocessing
from skimage import measure
from scipy.misc import imresize


def replace_img_format(file_basename):
    file_basename = file_basename.replace(".jpg", ".png")
    file_basename = file_basename.replace(".JPG", ".png")
    file_basename = file_basename.replace(".tif", ".png")
    return file_basename

    
def process(args):
    h_target, w_target, filenames, out_dir, out_maskdir = args
    for filename in filenames:
        # skip processed files
        out_file = os.path.join(out_dir, replace_img_format(os.path.basename(filename)))
        if os.path.exists(out_file):
            continue

        # skip if no optic disc (single value in mask)
        dir_mask = os.path.join(os.path.dirname(os.path.dirname(filename)), "masks")
        mask_fpath = os.path.join(dir_mask, filename)
        if not os.path.exists(mask_fpath):
            mask_fpath = os.path.join(dir_mask, os.path.basename(filename).replace(".jpg", ".tif"))
        if not os.path.exists(mask_fpath):
            mask_fpath = os.path.join(dir_mask, os.path.basename(filename).replace(".jpg", ".bmp"))
        if not os.path.exists(mask_fpath):
            mask_fpath = os.path.join(dir_mask, os.path.basename(filename).replace(".jpg", "_OD.tif"))
        if not os.path.exists(mask_fpath):
            print("skip {}".format(mask_fpath))
            continue
        ori_mask = np.array(Image.open(mask_fpath))
        if len(np.unique(ori_mask))<2:
            continue

        # read the image and resize 
        img = np.array(Image.open(filename))
        if img.shape[2] == 4:  # alpha channel
            img = img[..., :3]
        h_ori, w_ori, _ = np.shape(img)
        red_threshold = 40
        roi_check_len = h_ori // 5
        
        # Find Connected Components with intensity above the threshold
        blobs_labels, n_blobs = measure.label(img[:, :, 0] > red_threshold, return_num=True)
        if n_blobs == 0:
            print ("crop failed for %s " % (filename)
                    + "[no blob found]")
            continue
       
        # Find the Index of Connected Components of the Fundus Area (the central area)
        majority_vote = np.argmax(np.bincount(blobs_labels[h_ori // 2 - roi_check_len // 2:h_ori // 2 + roi_check_len // 2,
                                                                w_ori // 2 - roi_check_len // 2:w_ori // 2 + roi_check_len // 2].flatten()))
        if majority_vote == 0:
            print ("crop failed for %s " % (filename)
                    + "[invisible areas (intensity in red channel less than " + str(red_threshold) + ") are dominant in the center]")
            continue
        
        row_inds, col_inds = np.where(blobs_labels == majority_vote)
        row_max = np.max(row_inds)
        row_min = np.min(row_inds)
        col_max = np.max(col_inds)
        col_min = np.min(col_inds)
        if row_max - row_min < 100 or col_max - col_min < 100:
            print(n_blobs)
            for i in range(1, n_blobs + 1):
                print(len(blobs_labels[blobs_labels == i]))
            print("crop failed for %s " % (filename)
                    + "[too small areas]")
            continue
        
        # crop the image 
        crop_img = img[row_min:row_max, col_min:col_max]
        max_len = max(crop_img.shape[0], crop_img.shape[1])
        img_h, img_w, _ = crop_img.shape
        padded = np.zeros((max_len, max_len, 3), dtype=np.uint8)
        padded[(max_len - img_h) // 2:(max_len - img_h) // 2 + img_h, (max_len - img_w) // 2:(max_len - img_w) // 2 + img_w, ...] = crop_img
        resized_img = imresize(padded, (h_target, w_target), 'bicubic')
        
        # save cropped image
        Image.fromarray(resized_img).save(out_file)

        # change mask
        
        
        # skip existings
        out_file = os.path.join(out_maskdir, replace_img_format(os.path.basename(mask_fpath)))
        if os.path.exists(out_file):
            continue
        
        # handle mask
        mask=np.zeros(ori_mask.shape)
        if "AMD" in mask_fpath or "PALM" in mask_fpath or "REFUGE" in mask_fpath:
            mask[ori_mask!=255]=255
        else:
            mask[ori_mask==1]=255
        crop_mask = mask[row_min:row_max, col_min:col_max]
        max_len = max(crop_mask.shape[0], crop_mask.shape[1])
        img_h, img_w = crop_mask.shape
        padded = np.zeros((max_len, max_len), dtype=np.uint8)
        padded[(max_len - img_h) // 2:(max_len - img_h) // 2 + img_h, (max_len - img_w) // 2:(max_len - img_w) // 2 + img_w] = crop_mask
        resized_img = imresize(padded, (h_target, w_target), 'nearest')
        Image.fromarray(resized_img).save(out_file)


def image_shape(filename):
    img = Image.open(filename)
    img_arr = np.asarray(img)
    img_shape = img_arr.shape
    return img_shape


def image2arr(filename):
    img = Image.open(filename)
    img_arr = np.asarray(img)
    return img_arr


def all_files_under(path, extension=None, append_path=True, sort=True, contain_string=None):
    if append_path:
        if extension is None:
            if contain_string is None:
                filenames = [os.path.join(path, fname) for fname in os.listdir(path)]
            else:
                filenames = [os.path.join(path, fname) for fname in os.listdir(path) if contain_string in fname]
        else:
            if contain_string is None:
                filenames = [os.path.join(path, fname) for fname in os.listdir(path) if fname.endswith(extension)]
            else:
                filenames = [os.path.join(path, fname) for fname in os.listdir(path) if fname.endswith(extension) and contain_string in fname]
    else:
        if extension is None:
            if contain_string is None:
                filenames = [os.path.basename(fname) for fname in os.listdir(path)]
            else:
                filenames = [os.path.basename(fname) for fname in os.listdir(path) if contain_string in fname]
        else:
            if contain_string is None:
                filenames = [os.path.basename(fname) for fname in os.listdir(path) if fname.endswith(extension)]
            else:
                filenames = [os.path.basename(fname) for fname in os.listdir(path) if fname.endswith(extension) and contain_string in fname]
    if sort:
        filenames = sorted(filenames)
    
    return filenames


w_target, h_target = 640, 640
n_processes = 10
dirnames = ["AMD", "REFUGE", "PALM", "IDRiD_od_segmentation"]
dirnames = [ "IDRiD_od_segmentation"]

for dirname in dirnames:
    # make out_dir if not exists
    out_imgdir = dirname+"_640x640"
    out_maskdir = dirname+"_mask_640x640"
    os.makedirs(out_imgdir, exist_ok=True)
    os.makedirs(out_maskdir, exist_ok=True)
    
    # divide files
    all_files = all_files_under(os.path.join(dirname, "images"))
    filenames = [[] for __ in range(n_processes)]
    chunk_sizes = len(all_files) // n_processes
    for index in range(n_processes):
        if index == n_processes - 1:  # allocate ranges (last GPU takes remainders)
            start, end = index * chunk_sizes, len(all_files)
        else:
            start, end = index * chunk_sizes, (index + 1) * chunk_sizes
        filenames[index] = all_files[start:end]

    # run multiple processes
    pool = multiprocessing.Pool(processes=n_processes)
    pool.map(process, [(h_target, w_target, filenames[i], out_imgdir, out_maskdir) for i in range(n_processes)])
