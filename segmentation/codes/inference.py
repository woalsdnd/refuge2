# python inference.py --gpu_index=0 --img_dir=../../../data/REFUGE2/Validation-400-images/ --is_test=True
import os
import argparse
import time

import numpy as np
import pandas as pd
from PIL import Image

from model.efficient_net import EfficientNetB0, EfficientNetB4, set_output

import utils

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--gpu_index',
    type=str,
    required=True
    )
parser.add_argument(
    '--img_dir',
    type=str,
    required=True
    )
parser.add_argument(
    '--is_test',
    type=str,
    required=False,
    default="False"
    )    
FLAGS, _ = parser.parse_known_args()

# misc params
os.environ['CUDA_VISIBLE_DEVICES'] = FLAGS.gpu_index
vessel_segmentor_model_path = "/home/vuno/development/REFUGE2/fovea_localization/vessel_segmentation/model/vessel"
input_size = (640, 640, 3)
final_featuremap_resolution = 80

if FLAGS.is_test == "False":
    out_dir_home="/home/vuno/development/REFUGE2/segmentation/submission/disc_cup/refuge_finetune_ensemble"
else:
    out_dir_home="/home/vuno/development/REFUGE2/segmentation/submission/disc_cup/refuge_finetune_ensemble_test"
dict_weight_path = {seg_target:[] for seg_target in ["disc", "cup"]}
dict_weight_path["disc"] = ["/home/vuno/development/REFUGE2/segmentation/model/disc_refuge_finetune/weight_{}epoch.h5".format(epoch) for epoch in range(17, 21)] 
dict_weight_path["cup"] = ["/home/vuno/development/REFUGE2/segmentation/model/cup_refuge_finetune/weight_{}epoch.h5".format(epoch) for epoch in [5, 135]] 

out_pred = os.path.join(out_dir_home, "pred_map")
out_submission = os.path.join(out_dir_home, "final_mask")
if FLAGS.is_test == "False":
    for seg_target in ["disc", "cup"]:
        utils.makedirs(os.path.join(out_pred, seg_target))
utils.makedirs(out_submission)

# load a network
dict_network={seg_target:[] for seg_target in ["disc", "cup"]}
for seg_target in ["disc", "cup"]:
    for weight_path in dict_weight_path[seg_target]:
        fundus_network = EfficientNetB4(include_top=False, weights=None, use_resnet_init_conv=True, input_shape=input_size)
        vessel_network = EfficientNetB0(include_top=False, weights=None, use_resnet_init_conv=True, input_shape=input_size)
        network = set_output(fundus_network, vessel_network)
        network.load_weights(weight_path)
        print("model loaded from {}".format(weight_path))
        dict_network[seg_target].append(network)
vessel_segmentor = utils.load_network(vessel_segmentor_model_path)

# run inference
filepaths = utils.all_files_under(FLAGS.img_dir)
dict_thresholds = {"disc":180, "cup":180} # chosen by leaderboard best
for filepath in filepaths:
    print("processing {}...".format(filepath))

    # prepare input
    img = np.array(Image.open(filepath))
    h_ori, w_ori, _ = img.shape
    resized_img = utils.resize_img(img, input_size[0], input_size[1])
    standardized_resized_img = utils.standardize_fundus(resized_img)
    network_input = np.expand_dims(standardized_resized_img, axis=0)
    vessel_segmentation = vessel_segmentor.predict(network_input)
    
    # run 
    dict_list_mask = {seg_target:[] for seg_target in ["disc", "cup"]}
    for seg_target in ["disc", "cup"]:
        for network in dict_network[seg_target]:
            mask, mask_vessel = network.predict([network_input, np.repeat((vessel_segmentation * 255).astype(np.uint8) / 255., 3, axis=-1)])
            dict_list_mask[seg_target].append(mask)
    
    # resize prediction results
    dict_average_pred_mask = {}
    for seg_target in ["disc", "cup"]: 
        masks = np.array(dict_list_mask[seg_target])
        mask = np.mean(masks, axis=0)
        mask = mask[0,...,0] * 255
        mask = utils.resize_img(mask, h_ori, w_ori, method="bilinear")
        dict_average_pred_mask[seg_target]=mask
        if FLAGS.is_test == "False":
            Image.fromarray((mask).astype(np.uint8)).save(os.path.join(out_pred, seg_target, os.path.basename(filepath).replace(".jpg", ".png")))
        
    # postprocessing mask
    dict_post_processed_mask = {}
    for seg_target in ["disc", "cup"]: 
        mask = dict_average_pred_mask[seg_target]
        postprocessed_mask = np.copy(mask).astype(np.uint8)
        postprocessed_mask[mask>=dict_thresholds[seg_target]]=1
        postprocessed_mask[mask<dict_thresholds[seg_target]]=0
        if len(postprocessed_mask[postprocessed_mask==1]) > 0: # post-processing
            postprocessed_mask = utils.fill_holes(postprocessed_mask)
            postprocessed_mask = utils.remain_largest_blob(postprocessed_mask)
        dict_post_processed_mask[seg_target] = postprocessed_mask
    
    # generate final output
    result=np.ones(dict_post_processed_mask["cup"].shape)*255
    result[dict_post_processed_mask["disc"]==1]=128
    result[(dict_post_processed_mask["cup"]==1) & (dict_post_processed_mask["disc"]==1)]=0
    Image.fromarray(result.astype(np.uint8)).save(os.path.join(out_submission, os.path.basename(filepath).replace(".jpg", ".png")))
