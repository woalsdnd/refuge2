import multiprocessing
import threading
from queue import Queue
from uuid import uuid4
import os

import numpy as np
import dill


def subprocess(args):
    i, shared_list, datapoint, dumped_processing_func = args
    processing_func = dill.loads(dumped_processing_func)
    shared_list[i] = processing_func(datapoint)


class BatchFetcher(object):
    """
    Iterator for fetching batches in [batch_data1, batch_data2, ..., batch_data_K].
    
    Args:
        data (array-like): [data1, data2, ..., data_K]
        sample_weight (array-like): sampling weights for data 
        processing_func (function object): a function that handles data
        batch_size (int): the number of data in a batch
        sample (bool): whether to sample data while fetching 
        replace (bool): allow to sample same data
    
    Raises:
        AssertionError : data is not given as an array-like object
        AssertionError : data1, data2, ... do not have the same length
        
    """

    def __init__(self, data, sample_weight, processing_func, batch_size, sample, replace):
        assert isinstance(data, (list, tuple, np.ndarray))  # data should be given as an array-like object
        assert len(np.unique([len(d) for d in data])) == 1  # data1, data2, ... should have the same length
        
        self.n_data = len(data[0])
        self.data = data
        self.sample_weight = sample_weight
        self.processing_func = processing_func
        self.batch_size = batch_size
        self.sample = sample
        self.replace = replace
        self.n_itrs = int(np.ceil(1.*self.n_data / self.batch_size))

        self.pool = multiprocessing.Pool()
        
    def __iter__(self):
        if self.sample:
            indices = np.random.choice(np.arange(len(self.sample_weight)),
                                       size=len(self.sample_weight),
                                       replace=self.replace, p=self.sample_weight)
            data = [np.array(d)[indices] for d in self.data]
        else:
            data = self.data
            
        n_available_cpus = len(os.sched_getaffinity(0))
        queue = Queue(maxsize=n_available_cpus)
        end_marker = object()

        def enqueue():
            for i in range(self.n_itrs):
                data_batch = [np.array(d)[i * self.batch_size: (i + 1) * self.batch_size] for d in data]
                processed_data_batch = self._process(data_batch)
                queue.put(processed_data_batch)
            queue.put(end_marker)

        thread = threading.Thread(target=enqueue, daemon=True)
        thread.start()
        
        while True:
            item = queue.get()
            if item is end_marker:
                break
            else:
                yield item
                queue.task_done()    
                
    def _process(self, data):
        
        n_data = len(data[0])
        manager = multiprocessing.Manager()
        shared_list = manager.list([[]] * n_data)
        args = []
        for i in range(n_data):
            datapoint = [d[i] for d in data]
            args.append((i, shared_list, datapoint, dill.dumps(self.processing_func)))
        self.pool.map(subprocess, args)
        return [np.array(d) for d in zip(*shared_list)]  # [(data11, data12, data13), ...] => [[data11 data21 ...], ... ]

